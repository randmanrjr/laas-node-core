//Utility middleware for refreshing IDCS bearer token
var jwt = require('jsonwebtoken');
var fs = require('fs');
var https = require('https');
var queryString = require('querystring');
var StringDecoder = require('string_decoder').StringDecoder;
var idcsSigningCert = fs.readFileSync('IDCS_Signing_Key.cer');
var debug = require('debug')('LaaS:refresh.js');

//get refresh config from process.env
var refreshConfig = {
    idcs_hostname:          process.env.IDCS_HOSTNAME,
    client_id:              process.env.CLIENT_ID,
    client_secret:          process.env.CLIENT_SECRET,
    scope:                  process.env.IDCS_SCOPE
};

refreshIdcsToken = function (req,res,next) {

    //get a new idcsAccessToken via refresh_token grant flow
    function getIdcsAccessToken (req,res,next) {
        var options = {
            hostname: refreshConfig.idcs_hostname,
            path: '/oauth2/v1/token',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            auth: refreshConfig.client_id + ':' + refreshConfig.client_secret
        };
        var request = https.request(options, function (res) {
            debug('statusCode:', res.statusCode);
            debug('headers:', res.headers);
            var body = [];
            var decoder = new StringDecoder('utf8');
            res.on('data', function (chunk) {
                body.push(decoder.write(chunk));
            });

            res.on('end',function () {
                var _body = body.join('');
                debug('idcs response:',_body);
                try {
                    var bodyParsed = JSON.parse(_body);
                } catch (ex) {
                    console.error(ex);
                }
                debug('idcs body parsed:', bodyParsed);
                req.session.idcsAccessToken = bodyParsed.access_token;
                req.session.refreshToken = bodyParsed.refresh_token;
                next();

            });

        });
        debug('refresh request:',request);

        request.on('error', function (e) {
            console.error(e);
            //if error (unable to refresh bearer token) redirect to login
            res.redirect('/login');
        });

        var post_data = queryString.stringify({
            grant_type: 'refresh_token',
            refresh_token: req.session.refreshToken,
            scope: refreshConfig.scope
        });

        request.write(post_data);
        request.end();
    }

    //verify the idcsAccessToken
    jwt.verify(req.session.idcsAccessToken,
        idcsSigningCert,
        {algorithms:['RS256']},
        function (err, decoded) {
            if (err) {
                debug('invalid idcs token:',err);
                getIdcsAccessToken(req,res,next);
            } else {
                debug('valid idcs token:',decoded);
                next();
            }
        });
};

module.exports = refreshIdcsToken;