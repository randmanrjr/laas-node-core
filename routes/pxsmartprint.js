var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var https = require('https');
var base64 = require('base-64');
var queryString = require('querystring');
var StringDecoder = require('string_decoder').StringDecoder;
var debug = require('debug')('LaaS::pxsmartprint.js');
var fs = require('fs');
var jwt = require('jsonwebtoken');
var refreshIdcsToken = require('./utils/refresh');


var idcsConfigApi = {
    idcs_url:               process.env.IDCS_URL,
    idcs_hostname:          process.env.IDCS_HOSTNAME,
    client_id:              process.env.CLIENT_ID,
    client_secret:          process.env.CLIENT_SECRET,
    scope:                  process.env.API_SCOPE,
    grant_type:             process.env.API_GRANT_TYPE

};

var laasConfig = {
    laas_hostname:          process.env.LAAS_HOSTNAME
}

var idcsSigningCert = fs.readFileSync('IDCS_Signing_Key.cer');

router.use(function (req, res, next) {
    if (req.isUnauthenticated()) {res.redirect('/login')}
    next();
});

//refresh idcs bearer token if not valid
router.use(function (req, res, next) {
    refreshIdcsToken(req,res,next);
});

// middleware function to retrieve an additional bearer token with the API scope
function getApiAccessToken(req,res,next) {
    var _res = res;
    debug('getting api access token...');
    //verify id_token
    debug('verifying id_token...');
    jwt.verify(req.session.idToken,
        idcsSigningCert,
        {algorithms:['RS256']},
        function (err, decoded) {
            if (err) {
                debug('invalid id token:',err);
                res.redirect('/relogin'); // if id_token is invalid, have user re-authenticate
            } else {
                debug('valid id token:',decoded);
            }
        });

    var options = {
        hostname: idcsConfigApi.idcs_hostname,
        path: '/oauth2/v1/token',
        method: 'POST',
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Basic " + base64.encode(idcsConfigApi.client_id + ':' + idcsConfigApi.client_secret)
        }
    };
    var request = https.request(options, function (res) {
        debug('statusCode:', res.statusCode);
        debug('headers:', res.headers);

    });

    var post_data = queryString.stringify({
        grant_type: 'urn:ietf:params:oauth:grant-type:jwt-bearer',
        scope: idcsConfigApi.scope + ' offline_access', //offline_access scope provides refresh token
        assertion: req.session.idToken
    });

    request.write(post_data);

    request.on('response', function (response) {
        response.on('data', function (d) {
            var dts = d.toString();
            try {
                var dtsp = JSON.parse(dts);
                debug('data:', dtsp);
                req.session.apiAccessToken = dtsp.access_token;
                req.session.apiRefreshToken = dtsp.refresh_token;
                debug('api access token:', req.session.apiAccessToken);
                debug('api refresh token:', req.session.apiRefreshToken);
                next();
            } catch (ex) {
                debug('api access token res body json parse fail:',ex);
                res.redirect('/relogin');
            }
        });
    });

    request.on('error', function (e) {
        console.error(e);
        debug('api access token connection error');
        res.redirect('/relogin');
    });

    request.end();
}

//middleware function to refresh apiAccessToken
function refreshApiAccessToken(req,res,next) {
    var _res = res;
    var options = {
        hostname: idcsConfigApi.idcs_hostname,
        path: '/oauth2/v1/token',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Cache-Control': 'no-cache'
        },
        auth: idcsConfigApi.client_id + ':' + idcsConfigApi.client_secret
    };
    var request = https.request(options, function (res) {
        debug('statusCode:', res.statusCode);
        debug('headers:', res.headers);
        var body = [];
        var decoder = new StringDecoder('utf8');
        res.on('data', function (chunk) {
            body.push(decoder.write(chunk));
        });

        res.on('end',function () {
            var _body = body.join('');
            debug('api access refresh response:',_body);
            try {
                var bodyParsed = JSON.parse(_body);
                debug('api refresh body parsed:', bodyParsed);
                req.session.apiAccessToken = bodyParsed.access_token;
                req.session.apiRefreshToken = bodyParsed.refresh_token;
                next();
            } catch (ex) {
                debug('api access refresh body json parse fail:',ex);
                _res.redirect('/relogin');
            }
        });

    });
    debug('refresh request:',request);

    request.on('error', function (e) {
        console.error(e);
        debug('api access token refresh connection error');
        //if error (unable to refresh bearer token) have user re-authenticate
        res.redirect('/relogin');
    });

    var post_data = queryString.stringify({
        grant_type: 'refresh_token',
        refresh_token: req.session.apiRefreshToken,
        scope: idcsConfigApi.scope + ' offline_access'
    });

    debug('api refresh post_data:', post_data);
    request.write(post_data);
    request.end();
}

// middleware function to require apiAccessToken
function apiAccessToken(req, res, next) {
    if (!req.session.apiAccessToken) {
        getApiAccessToken(req,res,next);
    } else {
        jwt.verify(req.session.apiAccessToken,
            idcsSigningCert,
            {algorithms:['RS256']},
            function (err, decoded) {
                if (err) {
                    debug('invalid api token:',err);
                    refreshApiAccessToken(req,res,next);
                } else {
                    debug('valid api token:',decoded);
                    next();
                }
            });
    }
}

router.use(apiAccessToken);

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: false}));

router.post('/printers', function (req, res) {
    debug('printers req body',req.body);
    var queryPrint = '';

    if (req.body.site) {
        queryPrint = queryString.stringify({
            organization: req.body.organization,
            site: req.body.site
        });
    }


    var getPrinters = function (req) {
        //wrap the https call in a promise
        return new Promise(function (resolve, reject) {
            var options = {
                hostname:   laasConfig.laas_hostname,
                port:       443,
                path:       '/pxsmartprint/api/v1/printers?' + queryPrint,
                timeout:    10000,
                headers: {
                    "Authorization": "Bearer " + req.session.apiAccessToken
                }
            };
            var request = https.get(options, function (res) {

                res.on('data', function (d) {
                    resolve(d);
                });
            });

            request.on('error', function (e) {
                console.error(e);
                reject(e)
            });
            request.end();
        })
    };
    // settle the promise
    getPrinters(req).then(function (req) {
        debug('api res:', req.toString());
        var rts = req.toString();
        res.send(rts);
        return req;
    }).catch(function (req) {
        console.error(req);
        res.send(req);
    })
});

router.post('/labels', function (req, res) {

    debug('starting labels POST...');
    debug('req body',req.body);
    debug('req body.labels', req.body.labels);

    var queryPrintLabels = '';

    //Setup Request Body
    if (req.body.printer) {
        queryPrintLabels = JSON.stringify({
            jobName:        req.body.jobName,
            reference:      req.body.reference,
            printContext:   req.body.printContext,
            labels:         req.body.labels
        })
    }

    var postLabels = function (req) {
        //wrap the https call in a promise
        return new Promise(function (resolve, reject) {
            var printerName = encodeURIComponent(req.body.printer);
            //Setup options
            var options = {
                hostname:   laasConfig.laas_hostname,
                port:       443,
                path:       '/pxsmartprint/api/v1/labels?printer=' + printerName,
                timeout:    10000,
                method: 'POST',
                headers: {
                    "Authorization": "Bearer " + req.session.apiAccessToken,
                    "Content-Type" : "application/json",
                    "Accept" : "application/json"
                }
            };
            var request = https.request(options, function (res) {
                debug('statusCode:', res.statusCode);
                debug('headers:', res.headers);
                res.on('data', function (d) {
                    resolve(d);
                });
            });

            //Post the data/body
            debug('queryPrintLabels:',queryPrintLabels);
            request.write(queryPrintLabels);

            request.on('error', function (e) {
                console.error(e);
                reject(e)
            });
            request.end();
        })
    };
    // settle the promise
    postLabels(req).then(function (req) {
        debug('api res:', req.toString());
        var rts = req.toString();
        res.send(rts);
        return req;
    }).catch(function (req) {
        console.error(req);
        res.send(req);
    })
});

router.post('/jobs', function (req, res) {
    var queryPrint = '';

    if (req.body.operator) {
        queryPrint = queryString.stringify({
            operator: req.body.operator,
            maxJobs: 15,
            organization: req.body.organization,
            status:'ALL'
        });
    }

    var getJobStatus = function (req) {
        //wrap the https call in a promise
        return new Promise(function (resolve, reject) {
            var options = {
                hostname:   laasConfig.laas_hostname,
                port:       443,
                path:       '/pxsmartprint/api/v1/jobs?' + queryPrint,
                timeout:    10000,
                headers: {
                    "Authorization": "Bearer " + req.session.apiAccessToken
                }
            };
            var request = https.get(options, function (res) {

                var body = [];
                var decoder = new StringDecoder('utf8');
                res.on('data', function (chunk) {
                    debug('chunk:', decoder.write(chunk));
                    body.push(decoder.write(chunk));
                });

                res.on('end',function () {
                    var _body = body.join('');
                    resolve(_body);
                });
            });

            request.on('error', function (e) {
                console.error(e);
                reject(e)
            });
            request.end();
        })
    };
    // settle the promise
    getJobStatus(req).then(function (req) {
        debug('api res:', req.toString());
        var rts = req.toString();
        res.send(rts);
        return req;
    }).catch(function (req) {
        console.error(req);
        res.send(req);
    })
});

module.exports = router;