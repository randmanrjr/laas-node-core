var express = require('express');
var router = express.Router();
var debug = require('debug')('LaaS::index.js');
var https = require('https');
var IDCSStrategy = require('idcs-passport-authentication-strategy-randman');

var passportConfig = {
    idcs_url:               process.env.IDCS_URL,
    idcs_hostname:          process.env.IDCS_HOSTNAME,
    client_id:              process.env.CLIENT_ID,
    client_secret:          process.env.CLIENT_SECRET,
    callback_url:           process.env.CALLBACK_URL,
    post_logout_redirect:   process.env.LOGOUT_REDIRECT
};

// enforce authentication and then load front end application
router.get('/', function (req, res, next) {
    if (req.isUnauthenticated()) {res.redirect('/login')}
    next();
}, function(req, res) {
    res.sendFile('index.html', {root: './views'});
});

//logout endpoint
router.get('/logout', function (req, res) {
    var idToken = req.session.idToken;
    req.session.destroy();
    req.logOut();
    res.redirect(302, process.env.IDCS_URL + '/oauth2/v1/userlogout?' + 'post_logout_redirect_uri=' + process.env.LOGOUT_REDIRECT + '&id_token_hint=' + idToken);
    //res.redirect(302, IDCSStrategy.getLogoutURI(passportConfig));
    console.log('IDCS logout');
});

router.get('/relogin',function (req, res) {
    //make request to /oauth2/v1/userlogout
    var options = {
        hostname: process.env.IDCS_HOSTNAME,
        path: '/oauth2/v1/userlogout',
        method: 'GET',
        headers: {
            "Content-Type": "application/scim+json"
        }
    };
    var request = https.request(options, function (res) {
        debug('statusCode:', res.statusCode);
        debug('headers:', res.headers);
    });
    request.on('response', function (response) {
        debug('response:',response);
    });
    request.on('error',function (e) {
        console.error(e);
    });
    request.end();
    //destroy local session and logout
    req.session.destroy();
    req.logOut();
    res.redirect('/');
});

//get user info
router.post('/getuser', function (req, res) {
    debug('session.passport:', JSON.stringify(req.session.passport, null,2));
    res.send(req.session.passport);
});

module.exports = router;
