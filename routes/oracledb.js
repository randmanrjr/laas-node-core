var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var https = require('https');
var queryString = require('querystring');
var StringDecoder = require('string_decoder').StringDecoder;
var moment = require('moment');
var debug = require('debug')('LaaS::oracledb.js');
var refreshIdcsToken = require('./utils/refresh');

var ordsConfig = {
    ords_hostname:                  process.env.ORDS_HOSTNAME,
    ords_path:                      process.env.ORDS_PATH,
    ords_client_id:                 process.env.ORDS_CLIENT_ID,
    ords_client_secret:             process.env.ORDS_CLIENT_SECRET,
    ords_print_history_path:        process.env.ORDS_PRINT_HISTORY_PATH,
    ords_product_data_path:         process.env.ORDS_PRODUCT_DATA_PATH,
    ords_post_print_history_path:   process.env.ORDS_POST_PRINT_HISTORY_PATH
};

var tokenFail = 0;

router.use(function (req, res, next) {
    if (req.isUnauthenticated()) {res.redirect('/relogin')}
    next();
});

//refresh idcs bearer token if not valid
router.use(function (req, res, next) {
    refreshIdcsToken(req,res,next);
});

function getOrdsAccessToken (req,res,next) {
    debug('getting ords access token...');
    var _res = res;
    var options = {
        hostname: ordsConfig.ords_hostname,
        path: ordsConfig.ords_path,
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        auth: ordsConfig.ords_client_id + ':' + ordsConfig.ords_client_secret
    };
    var request = https.request(options, function (res) {
        debug('statusCode:', res.statusCode);
        debug('headers:', res.headers);
        var body = [];
        var decoder = new StringDecoder('utf8');
        res.on('data', function (chunk) {
            body.push(decoder.write(chunk));
        });

        res.on('end',function () {
            var _body = body.join('');
            debug('ords response:',_body);
            try {
                var bodyParsed = JSON.parse(_body);
                debug('ords body parsed:', bodyParsed);
                req.session.ordsAccessToken = bodyParsed.access_token;
                req.session.ordsAccessTokenTimeStamp = moment();
                debug('ords access token:', req.session.ordsAccessToken);
                debug('ords access token time stamp:',req.session.ordsAccessTokenTimeStamp);
                next();
            } catch (ex) {
                console.error(ex);
                tokenFail++;
                debug('response from ORDS is not json');
                if (tokenFail > 1) {
                    _res.status(500).end(); //break out of recursion
                } else {
                    getOrdsAccessToken(req,_res,next);
                }

            }


        });

    });
    debug('request:',request);

    request.on('error', function (e) {
        console.error(e);
        debug('ORDS access token connection error');
        _res.status(500).end();
    });

    var post_data = queryString.stringify({
        grant_type: 'client_credentials'
    });

    request.write(post_data);
    request.end();
}

function oracledbApiAccess(req, res, next) {

    debug('oracledbApiAccess');

    if (!req.session.ordsAccessToken) {
        debug('no ordsAccessToken...');
        getOrdsAccessToken(req,res,next)
    } else {
        debug('has ords token...');
        var duration = moment.duration({'minutes': 58});
        var now = moment();
        var durationPlusLastOrdsTimeStamp = moment(req.session.ordsAccessTokenTimeStamp).add(duration);
        if (now.isAfter(durationPlusLastOrdsTimeStamp,'minute')) {
            debug('ords token is expired...');
            getOrdsAccessToken(req,res,next);
        } else {
            next();
        }
    }
}

router.use(oracledbApiAccess);

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: false}));

router.post('/printhistorysearch', function (req, res, next) {

    var _queryPrint = encodeURI(JSON.stringify({
        product_code: req.body.part,
        lot_number: req.body.lot
    }));
    debug('_queryprint',_queryPrint);

    var getHistoryRecords = function (req,res,next) {
        //wrap the https call in a promise
        return new Promise(function (resolve, reject) {
            var options = {
                hostname:   ordsConfig.ords_hostname,
                port:       443,
                path:       ordsConfig.ords_print_history_path + '?q=' + _queryPrint,
                timeout:    10000,
                headers: {
                    "Authorization": "Bearer " + req.session.ordsAccessToken
                }
            };
            var request = https.get(options, function (res) {
                debug('statusCode:', res.statusCode);
                debug('headers:', res.headers);
                var body = [];
                var decoder = new StringDecoder('utf8');
                res.on('data', function (chunk) {
                    debug('chunk:', decoder.write(chunk));
                    body.push(decoder.write(chunk));
                });

                res.on('end',function () {
                    var _body = body.join('');
                    resolve(_body);
                });
            });

            debug('request:', request);


            request.on('error', function (e) {
                console.error(e);
                reject(e)
            });
            request.end();
        })
    };

    // settle the promise
    getHistoryRecords(req).then(function (req) {
        debug('oracledb_api get history res:', req);
        res.send(req);
        return req;
    }).catch(function (req) {
        debug('oracledb get history rest err:',req.e);
        //error object that ducks types the ORDS response
        var errorResponseObj = {"items": [], "error":req.e};
        res.send(errorResponseObj);
        return errorResponseObj;
    })

});


router.post('/labeldata', function (req, res, next) {

    debug('calling product label data API...');

    var _queryHistory = encodeURI(JSON.stringify({
        supplier: req.body.supplier,
        product_code: req.body.part
    }));
    console.log('_queryprint',_queryHistory);

    var getLabelRecords = function (req,res,next) {
        //wrap the https call in a promise
        return new Promise(function (resolve, reject) {
            var options = {
                hostname:   ordsConfig.ords_hostname,
                port:       443,
                path:       ordsConfig.ords_product_data_path+ '?q=' + _queryHistory,
                timeout:    10000,
                headers: {
                    "Authorization": "Bearer " + req.session.ordsAccessToken
                }
            };
            var request = https.get(options, function (res) {
                debug('statusCode:', res.statusCode);
                debug('headers:', res.headers);
                var body = [];
                var decoder = new StringDecoder('utf8');
                res.on('data', function (chunk) {
                    debug('chunk:', decoder.write(chunk));
                    body.push(decoder.write(chunk));
                });

                res.on('end',function () {
                    var _body = body.join('');
                    resolve(_body);
                });
            });

            debug('request:', request);


            request.on('error', function (e) {
                console.error(e);
                reject(e)
            });
            request.end();
        })
    };

    // settle the promise
    getLabelRecords(req).then(function (req) {
        debug('oracledb_api res:', req);
        res.send(req);
        return req;
    }).catch(function (req) {
        debug('oracledb rest err:',req.e);
        //error obj that duck types the ORDS response
        var errorResponseObj = {"items": [], "error":req.e};
        res.send(errorResponseObj);
        return errorResponseObj;
    })

});

router.post('/printhistory', function (req, res) {

    debug('calling print history insert API...');
    var dataList=[];

    for(var property in req.body) {
        var reqItem={};
        reqItem[property] = req.body[property];
        dataList.push(reqItem);
    }


    /*var dataHistoryInsert = JSON.stringify({
        productCode: req.body.productCode,
        isReprint:req.body.isReprint,
        lotNumber: req.body.lotNumber,
        plrID: req.body.plrID,
        productIdentifier: req.body.productIdentifier,
        manufactureDate:req.body.manufactureDate,
        expirationDate:req.body.expirationDate,
        countryOfOriginID:req.body.countryOfOriginID,
        variant:req.body.variant,
        printUser:req.body.printUser,
        organization:req.body.organization,
        lotQty:req.body.lotQty,
        historyDetails:req.body.historyDetails,
        jobID: req.body.jobID,
        batchID: req.body.batchID
    });*/

    dataHistoryInsert = JSON.stringify(dataList);
    debug('dataHistoryInsert',dataHistoryInsert);

    var postPrintHistory = function (req) {
        //wrap the https call in a promise
        return new Promise(function (resolve, reject) {
            var options = {
                hostname:   ordsConfig.ords_hostname,
                port:       443,
                path:       ordsConfig.ords_post_print_history_path,
                timeout:    10000,
                method:     'POST',
                headers: {
                    "Authorization": "Bearer " + req.session.ordsAccessToken,
                    "Content-Type" : "application/json",
                    "Accept" : "application/json"
                }
            };
            var request = https.request(options, function (res) {
                debug('statusCode:', res.statusCode);
                debug('headers:', res.headers);
                /*res.on('data', function (d) {
                    resolve(d);
                });*/
                var body = [];
                var decoder = new StringDecoder('utf8');
                res.on('data', function (chunk) {
                    debug('chunk:', decoder.write(chunk));
                    body.push(decoder.write(chunk));
                });

                res.on('end',function () {
                    var _body = body.join('');
                    resolve(_body);
                });
            });

            //Post the data/body
            request.write(dataHistoryInsert);

            request.on('error', function (e) {
                console.error('API Print History Error',e);
                reject(e)
            });
            request.end();

        })
    };

    // settle the promise
    postPrintHistory(req).then(function (req) {
        var rts = req.toString();
        var successText="Successfully Inserted Print History Data";
        if (res.statusCode === 200){
            //Populate our own object for response, as ORDS returns nothing on success
            res.send({"status":successText});
            return(res);
        }
        else{
            res.send(rts);
            return req;
        }


    }).catch(function (req) {
        debug('oracledb rest err:',req);
        var errorText = "Print History Connection Error";
        res.send({"status": errorText});
    })

});

module.exports = router;