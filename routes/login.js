var express = require('express');
var router = express.Router();
var passport = require('passport');
var bodyParser = require('body-parser');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: false}));

// Use IDCS login page
router.get('/', passport.authenticate(['idcs-openid']));

router.get('/callback', passport.authenticate(['idcs-openid'], { failureRedirect: '/login' }),
    function(req, res) {
        res.redirect('/');
        }
);

module.exports = router;
