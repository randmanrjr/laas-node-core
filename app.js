var express = require('express'); // Express web application framework
var helmet = require('helmet'); // Helmet is a security middleware that ensures that http headers are correctly set
var path = require('path'); // file path utility
var logger = require('morgan'); // logging middleware for Express
var cookieParser = require('cookie-parser'); // Cookie parsing middleware for Express
var bodyParser = require('body-parser'); // Express request body parsing middleware
var passport = require('passport'); // Express authentication middleware; supports many authentication strategies, including OAuth 2
var IDCSStrategy = require('idcs-passport-authentication-strategy-randman');
var session = require('express-session'); // Express session middleware; required for maintaining web sessions
var index = require('./routes/index'); // index router
var login = require('./routes/login'); // login router
var pxSmartPrintApi = require('./routes/pxsmartprint'); // pxsmartprint api endpoint router
var oracleDBApi = require('./routes/oracledb'); // oracledb api endpoint router
var debug = require('debug')('LaaS::app.js');

// app bootstrapping
var app = express();

var passportConfig = {
    idcs_url :              process.env.IDCS_URL,
    profile_url:            process.env.PROFILE_URL,
    client_id:              process.env.CLIENT_ID,
    client_secret:          process.env.CLIENT_SECRET,
    scope:                  process.env.IDCS_SCOPE,
    callback_url:           process.env.CALLBACK_URL,
    post_logout_redirect:   process.env.LOGOUT_REDIRECT
};
debug('passportConfig: ', passportConfig);

passport.use(new IDCSStrategy(passportConfig, function(req, accessToken, refreshToken, idToken, profile, done) {
    console.log('idcs-openid-init');
    req.session.idcsAccessToken = accessToken;
    req.session.idToken = idToken;
    req.session.refreshToken = refreshToken;
    debug('access_token: ', accessToken);
    debug('refresh token:', refreshToken);
    debug('id_token: ', idToken);
    return done(null, profile);
}));

passport.serializeUser(function(user, done) {
    done(null, user);
});

passport.deserializeUser(function(user, done) {
    done(null, user);
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// middleware stack handles req and res bodies in sequence
app.use(logger('dev'));
app.use(helmet());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({secret: "DEMO_SECRET_VALUE", resave:true, saveUninitialized:true}));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

//prevent browser caching
app.use(function (req, res, next) {
    res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    res.header('Expires', '-1');
    res.header('Pragma', 'no-cache');
    next()
});

app.use('/', index); // index router middleware
app.use('/login', login); //login router middleware
app.use('/api/v1', pxSmartPrintApi); // api endpoint middleware
app.use('/oracledb/api/v1', oracleDBApi); // oracledb api endpoints

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
